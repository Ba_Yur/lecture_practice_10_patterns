void main(List<String> arguments) {}

class HelperSingleton {
  static final HelperSingleton _singleton = HelperSingleton._internal();

  factory HelperSingleton() {
    return _singleton;
  }

  HelperSingleton._internal();
}

class User {
  final String name;
  final String id;
  final String email;
  final String phone;

  User({this.name, this.id, this.email, this.phone});

  User copyWithUser(newUser) {
    return User(
      name: newUser.name ?? name,
      id: newUser.id ?? id,
      email: newUser.email ?? email,
      phone: newUser.phone ?? phone,
    );
  }
}

abstract class DecoratorAnimal {
  String voice();
}

class Cat implements DecoratorAnimal {
  @override
  String voice() => 'Myau';
}

// adapter

class Size {
  final int width;
  final int height;

  Size(this.width, this.height);

  void getSquare() {
    print('${width * height}');
  }
}

class IncreasedSize {
  Size _size;

  IncreasedSize(int width, int height) {
    _size = Size(width, height);
  }

  void getSquare() => _size.getSquare();
}

//builder

enum carColor { red, blue, white }

enum carDoorsNum { two, three, four }

class CarBuilder {
  carColor color;
  carDoorsNum doorNum;
}

class Car {
  final carColor color;
  final carDoorsNum doorsNum;

  Car(CarBuilder builder)
      : color = builder.color,
        doorsNum = builder.doorNum;
}



